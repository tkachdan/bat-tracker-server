package utils;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.GsonBuilder;
import constants.Constants;
import persistence.model.Intersection;
import persistence.model.NotificationType;
import persistence.model.UserEntity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Notification library. Provides sending notification capabilities to different devices using GCM.
 */
public class NotificationUtil {
    private static NotificationUtil ourInstance = new NotificationUtil();

    public static NotificationUtil getInstance() {
        return ourInstance;
    }

    private NotificationUtil() {
    }


    /**
     * Send update info for all users in collection.
     * @param users
     */
    public synchronized void sendInfoNotificationToUsers(Collection<UserEntity> users) {
        Sender sender = new Sender(Constants.APIKey);
        try {
            Message message = new Message.Builder()
                    .addData("type", NotificationType.INFO.toString())
                    .build();
            for (UserEntity user : users) {
                Result result = sender.send(message, user.getDeviceId(), 2);
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Send update info for all users in collection.
     * @param users
     */
    public synchronized void sendStopNotificationToUsers(Collection<UserEntity> users) {
        Sender sender = new Sender(Constants.APIKey);
        try {
            Message message = new Message.Builder()
                    .addData("type", NotificationType.STOP.toString())
                    .build();
            for (UserEntity user : users) {
                Result result = sender.send(message, user.getDeviceId(), 2);
                System.out.println(result);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Send final intersection coordinates to users.
     * @param users
     * @param intersection
     */
    public synchronized void sendIntersectionToUsers(Collection<UserEntity> users, Intersection intersection) {
        Sender sender = new Sender(Constants.APIKey);
        System.out.println("Intersection: " + intersection.toString());
        System.out.println(users.toString());
        Message message = new Message.Builder()
                .addData("type", NotificationType.INTERSECTION.name())
                .addData("id", intersection.getId()+"")
                .addData("latitude", intersection.getLatitude()+"")
                .addData("longitude", intersection.getLongitude()+"")
                .addData("timeStamp", new SimpleDateFormat(Constants.DATE_FORMAT).format(intersection.getTimeStamp()))
                .build();
        for (UserEntity user : users) {
            try {
                Result result = sender.send(message, user.getDeviceId(), 2);
                System.out.println(result);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send notification that indicates that measurement was started. With interval.
     * @param users
     * @param nextMeasurement
     * @param interval
     */
    public synchronized void sendStartNotificationToUsers(Collection<UserEntity> users, Date nextMeasurement, int interval) {
        Sender sender = new Sender(Constants.APIKey);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        try {
            Message message = new Message.Builder()
                    .addData("type", NotificationType.START.toString())
                    .addData("nextMeasurement", simpleDateFormat.format(nextMeasurement))
                    .addData("interval", String.valueOf(interval))
                    .build();
            for (UserEntity user : users) {
                Result result = sender.send(message, user.getDeviceId(), 2);
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send notification that measurement has been started. Without interval
     * @param connectedUsers
     * @param nextMeasurement
     */
    public void sendStartNotificationToUsers(List<UserEntity> connectedUsers, Date nextMeasurement) {
        Sender sender = new Sender(Constants.APIKey);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Message message = new Message.Builder()
                    .addData("type", NotificationType.UPDATE.toString())
                    .addData("nextMeasurement", simpleDateFormat.format(nextMeasurement))
                    .build();
            for (UserEntity user : connectedUsers) {
                Result result = sender.send(message, user.getDeviceId(), 2);
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
