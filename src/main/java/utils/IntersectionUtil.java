package utils;

import org.primefaces.model.map.LatLng;
import persistence.model.UserEntity;

import java.util.Collection;

/**
 * Mathematical library for calculating intersections. Singleton
 */
public class IntersectionUtil {
    private static IntersectionUtil ourInstance = new IntersectionUtil();

    public static IntersectionUtil getInstance() {
        return ourInstance;
    }

    private IntersectionUtil() {
    }

    /**
     * Calculate intersection of centroid.
     * @param users
     * @return
     */
    public LatLng calculateCentroid(Collection<UserEntity> users) {
        double lat = 0.;
        double lng = 0.;
        for (UserEntity user : users) {
            lat += user.getLatitude();
            lng += user.getLongitude();
        }

        lat = lat / users.size();
        lng = lng / users.size();

        return new LatLng(lat, lng);
    }

    /**
     * Calculate intersection of two lines.
     * @param pos1StartX
     * @param pos1StartY
     * @param pos1EndX
     * @param pos1EndY
     * @param pos2StartX
     * @param pos2StartY
     * @param pos2EndX
     * @param pos2EndY
     * @return Intersecion coordinates
     */
    public LatLng coordinateOfIntersectionOf2Lines(double pos1StartX,
                                                   double pos1StartY, double pos1EndX, double pos1EndY,
                                                   double pos2StartX, double pos2StartY, double pos2EndX,
                                                   double pos2EndY) {

        if ((((pos1StartX - pos1EndX) * (pos2StartY - pos2EndY))
                - ((pos1StartY - pos1EndY) * (pos2StartX - pos2EndX))) == 0) {
            return null;
        }
        double x = ((((pos1StartX * pos1EndY) - (pos1StartY * pos1EndX))
                * (pos2StartX - pos2EndX)) - ((pos1StartX - pos1EndX)
                * ((pos2StartX * pos2EndY) - (pos2StartY * pos2EndX))))
                / (((pos1StartX - pos1EndX) * (pos2StartY - pos2EndY))
                - ((pos1StartY - pos1EndY) * (pos2StartX - pos2EndX)));

        double y = ((((pos1StartX * pos1EndY) - (pos1StartY * pos1EndX))
                * (pos2StartY - pos2EndY)) - ((pos1StartY - pos1EndY)
                * ((pos2StartX * pos2EndY) - (pos2StartY * pos2EndX))))
                / (((pos1StartX - pos1EndX) * (pos2StartY - pos2EndY))
                - ((pos1StartY - pos1EndY) * (pos2StartX - pos2EndX)));

        return new LatLng(x, y);
    }


    public LatLng coordinateOfIntersectionOf2Lines(LatLng point1S,
                                                   LatLng point1E, LatLng point2S, LatLng point2E) {
        return coordinateOfIntersectionOf2Lines(point1S.getLat(),
                point1S.getLng(), point1E.getLat(), point1E.getLng(),
                point2S.getLat(), point2S.getLng(), point2E.getLat(),
                point2E.getLng());
    }


    /**
     * Calculate coordinates from distance and azimth.
     * @param latitude
     * @param longitude
     * @param distance
     * @param azimuth
     * @return
     */
    public LatLng getCoordinatesFromDistanceAndAzimuth(double latitude, double longitude
            , int distance, float azimuth) {
        double earthRadius = 6378.1;
        double constToRadian = Math.PI / 180;
        double constToDegrees = 180 / Math.PI;
        double directionRadian = constToRadian * azimuth;
        double distanceToEarthRatio = distance / earthRadius;

        double latitudeRadians = latitude * constToRadian;
        double longitudeRadians = longitude * constToRadian;

        double destLatitude = Math.asin(Math.sin(latitudeRadians)
                * Math.cos(distanceToEarthRatio) + Math.cos(latitudeRadians)
                * Math.sin(distanceToEarthRatio) * Math.cos(directionRadian));

        double destLongtitude = longitudeRadians
                + Math.atan2(
                Math.sin(directionRadian)
                        * Math.sin(distanceToEarthRatio)
                        * Math.cos(latitudeRadians),
                Math.cos(distanceToEarthRatio)
                        - Math.sin(latitudeRadians)
                        * Math.sin(destLatitude));

        destLatitude = destLatitude * constToDegrees;
        destLongtitude = destLongtitude * constToDegrees;

        return new LatLng(destLatitude, destLongtitude);
    }


}
