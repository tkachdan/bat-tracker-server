package services.impl;

import persistence.DAO.Impl.AdventureDAOImpl;
import persistence.model.Adventure;
import services.AdventureService;
import java.util.Collection;

/**
 * Created by martin on 4.3.15.
 */
public class AdventureServiceImpl implements AdventureService {

    AdventureDAOImpl adventureDAO = new AdventureDAOImpl();

    private boolean hasWrongArguments(Adventure adventure) {
        return (adventure == null
                || adventure.getOwner() == null
                || adventure.getName().trim().equals("")
                || adventure.getSpecies().trim().equals("")
                || adventure.getStartDate() == null
        );
    }

    @Override
    public int createAdventure(Adventure adventure) {
        if (hasWrongArguments(adventure)) {
            return -1;
        } else {
            return adventureDAO.createObject(adventure);
        }
    }

    @Override
    public Adventure readAdventureByid(int id) {
        return adventureDAO.readObjectById(id);
    }

    @Override
    public boolean updateAdventure(Adventure adventure) {
        return false;
    }

    @Override
    public boolean deleteAdventure(Adventure adventure) {
        return false;
    }

    @Override
    public Collection<Adventure> readAllAdventures() {
        return adventureDAO.getAllObjects();
    }

}
