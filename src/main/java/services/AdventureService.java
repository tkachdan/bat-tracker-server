package services;

import persistence.model.Adventure;
import java.util.Collection;

/**
 * Created by martin on 4.3.15.
 */
public interface AdventureService {

    public int createAdventure(Adventure adventure);

    public Adventure readAdventureByid(int id);

    public boolean updateAdventure(Adventure adventure);

    public boolean deleteAdventure(Adventure adventure);

    public Collection<Adventure> readAllAdventures();

}
