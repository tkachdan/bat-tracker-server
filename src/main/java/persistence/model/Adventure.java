package persistence.model;

import constants.Constants;
import org.hibernate.HibernateError;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by dan on 26.2.15.
 *
 */
@Entity
public class Adventure extends AbstractEntity {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "species")
    private String species;

    @Column(name = "startDate")
    private Date startDate;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Intersection> intersections;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<UserEntity> allowedUsers;

    @OneToOne
    private Measurement measurement;

    @OneToOne()
    @NotNull
    private UserEntity owner;

    public Adventure() {
    }

    public Adventure(String name, String species, Date startDate,
                     UserEntity owner) {
        this.name = name;
        this.species = species;
        this.startDate = startDate;
        this.owner = owner;
        allowedUsers = new HashSet<UserEntity>();
        intersections = new HashSet<Intersection>();
        if (owner == null) {
            throw new HibernateError("No owner!");
        }
        measurement = null;
    }

    public void addAllowedUsers(UserEntity userEntity) {
        allowedUsers.add(userEntity);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        if (owner == null) {
            throw new HibernateError("No owner!");
        } else {
            this.owner = owner;
        }
    }

    public Set<Intersection> getIntersections() {
        return intersections;
    }

    public void setIntersections(Set<Intersection> intersections) {
        this.intersections = intersections;
    }

    public void addIntersection(Intersection intersection) {
        this.intersections.add(intersection);
    }

    public Set<UserEntity> getAllowedUsers() {
        return allowedUsers;
    }

    public void setAllowedUsers(Set<UserEntity> allowedUsers) {
        this.allowedUsers = allowedUsers;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Adventure adventure = (Adventure) o;

        if (id != adventure.id) return false;
        if (allowedUsers != null ? !allowedUsers.equals(adventure.allowedUsers) : adventure.allowedUsers != null)
            return false;
        if (intersections != null ? !intersections.equals(adventure.intersections) : adventure.intersections != null)
            return false;
        if (measurement != null ? !measurement.equals(adventure.measurement) : adventure.measurement != null)
            return false;
        if (name != null ? !name.equals(adventure.name) : adventure.name != null) return false;
        if (owner != null ? !owner.equals(adventure.owner) : adventure.owner != null) return false;
        if (species != null ? !species.equals(adventure.species) : adventure.species != null) return false;
        if (startDate != null ? !startDate.equals(adventure.startDate) : adventure.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (species != null ? species.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (intersections != null ? intersections.hashCode() : 0);
        result = 31 * result + (allowedUsers != null ? allowedUsers.hashCode() : 0);
        result = 31 * result + (measurement != null ? measurement.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Adventure{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", species='" + species + '\'' +
                ", startDate=" + startDate +
                ", intersections=" + intersections +
                ", allowedUsers=" + allowedUsers +
                ", measurement=" + measurement +
                ", owner=" + owner +
                '}';
    }
}
