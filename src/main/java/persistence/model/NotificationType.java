package persistence.model;

/**
 * Created by dan on 28.3.15.
 */
public enum NotificationType {
    START,
    UPDATE,
    INTERSECTION,
    STOP,
    INFO
}
