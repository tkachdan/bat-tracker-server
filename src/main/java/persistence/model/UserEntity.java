package persistence.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dan on 26.2.15.
 */

@Entity
public class UserEntity extends AbstractEntity {

    @Id
    @GeneratedValue
    private int id;
    @Column(unique = true)
    private String email;
    private String password;
    private boolean isMeasuring;
    private int connectedAdventureId;
    private double latitude;
    private double longitude;
    private double azimuth;
    private String deviceId;

    public UserEntity() {
    }

    public UserEntity(String email, String password) {
        this.email = email;
        this.password = password;
        latitude = -1;
        longitude = -1;
        azimuth = -1;
        isMeasuring = false;
        connectedAdventureId = -1;
        deviceId = null;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isMeasuring() {
        return isMeasuring;
    }

    public void setMeasuring(boolean isMeasuring) {
        this.isMeasuring = isMeasuring;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(double azimuth) {
        this.azimuth = azimuth;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getConnectedAdventureId() {
        return connectedAdventureId;
    }

    public void setConnectedAdventureId(int connectedAdventureId) {
        this.connectedAdventureId = connectedAdventureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;
        if (id != that.id) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = id;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", isMeasuring=" + isMeasuring +
                ", connectedAdventureId=" + connectedAdventureId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", azimuth=" + azimuth +
                ", deviceId='" + deviceId + '\'' +
                '}';
    }
}
