package persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by dan on 26.2.15.
 */
@Entity
public class Intersection extends AbstractEntity {
    @Id
    @GeneratedValue
    private int id;

    private double latitude;

    private double longitude;

    private Date timeStamp;

    public Intersection() {

    }

    public Intersection(double latitude, double longitude, Date timeStamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timeStamp = timeStamp;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double posX) {
        this.latitude = posX;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double posY) {
        this.longitude = posY;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date time) {
        this.timeStamp = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Intersection that = (Intersection) o;

        if (id != that.id) return false;
        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        if (!timeStamp.equals(that.timeStamp)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + timeStamp.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Intersection{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
