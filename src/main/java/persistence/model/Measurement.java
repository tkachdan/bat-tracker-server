package persistence.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by dan on 16.3.15.
 */
@Entity
public class Measurement extends AbstractEntity {

    @Id
    @GeneratedValue
    private int id;
    private int adventureId;
    private Date startDate;
    private int interval;


    public Measurement() {
    }

    public Measurement(Date startDate, int interval) {
        this.startDate = startDate;
        this.interval = interval;
    }

    public Measurement(int adventureId, Date startDate, int interval) {
        this.adventureId = adventureId;
        this.startDate = startDate;
        this.interval = interval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdventureId() {
        return adventureId;
    }

    public void setAdventureId(int adventureId) {
        this.adventureId = adventureId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStartDate(long milis) {
        startDate.setTime(milis);
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Measurement that = (Measurement) o;

        if (adventureId != that.adventureId) return false;
        if (id != that.id) return false;
        if (interval != that.interval) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + adventureId;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + interval;
        return result;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "id=" + id +
                ", adventureId=" + adventureId +
                ", startDate=" + startDate +
                ", interval=" + interval +
                '}';
    }
}

