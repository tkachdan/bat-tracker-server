package persistence.DAO.Impl;

import org.hibernate.Session;
import persistence.DAO.DAO;
import persistence.DAO.DAO1;
import utils.HibernateService;

import java.util.Collection;
import java.util.List;

/**
 * Created by dan on 1.6.15.
 */
public class GenericDAOImpl<T> implements DAO1<T> {


    @Override
    public T createObject(T t) {
        Session session = HibernateService.getSession();
        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();
        session.close();
        return t;
    }

    @Override
    public T readObjectById(int id, Class<T> tClass) {
        Session session = HibernateService.getSession();
        session.beginTransaction();


        T t = (T) session
                .createQuery("from " + tClass.getSimpleName() + " where id = :itemId")
                .setLong("itemId", id).uniqueResult();

        session.getTransaction().commit();
        session.close();
        return t;
    }

    @Override
    public boolean updateObject(T t) {
        Session session = HibernateService.getSession();
        session.beginTransaction();

        session.update(t);
        session.getTransaction().commit();
        session.close();
        return true;
    }

    @Override
    public boolean deleteObject(int id, Class<T> tClass) {
        Session session = HibernateService.getSession();
        session.beginTransaction();

        T t = (T) session.load(tClass, id);
        session.delete(t);
        session.getTransaction().commit();
        session.close();
        return false;
    }

    public boolean isObjectInDatabase(int id, Class<T> tClass) {
        if (this.readObjectById(id, tClass) == null)
            return false;
        else
            return true;
    }

    public Collection<T> getAllObjects(Class<T> tClass) {
        Session session = HibernateService.getSession();
        session.beginTransaction();

        List objects = session.createQuery("from " + tClass.getSimpleName()).list();

        session.close();
        return objects;
    }
}
