package persistence.DAO.Impl;

import org.hibernate.Query;
import org.hibernate.Session;
import persistence.model.UserEntity;
import utils.HibernateService;

import javax.jws.soap.SOAPBinding;
import java.util.Collection;
import java.util.List;

/**
 * Created by dan on 26.2.15.
 */
public class UserDAOImpl extends DAOImpl<UserEntity> {

    @Override
    public int createObject(UserEntity userEntity) {
        return super.createObject(userEntity);
    }

    public UserEntity readObjectById(int id) {
        return super.readObjectById(id, UserEntity.class);
    }

    public UserEntity readObjectByEmail(String email) {
        Session session = HibernateService.getSession();
        session.beginTransaction();

        String hql = "from UserEntity u where u.email=:userEmail";
        Query query = session.createQuery(hql);
        query.setParameter("userEmail", email);
        List results = query.list();
        session.close();
        if(results.size()==0){
            return null;
        }

        return (UserEntity)results.get(0);
    }

    @Override
    public boolean updateObject(UserEntity userEntity) {
        return super.updateObject(userEntity);
    }

    public boolean deleteObject(int id) {
        return super.deleteObject(id, UserEntity.class);
    }

    public boolean isObjectInDatabase(int id) {
        return super.isObjectInDatabase(id, UserEntity.class);
    }


    public Collection<UserEntity> getAllObjects() {
        return super.getAllObjects(UserEntity.class);
    }
}
