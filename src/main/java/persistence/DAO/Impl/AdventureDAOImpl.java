package persistence.DAO.Impl;

import org.hibernate.Query;
import org.hibernate.Session;
import persistence.model.Adventure;
import persistence.model.UserEntity;
import utils.HibernateService;

import java.util.Collection;
import java.util.List;

/**
 * Created by dan on 26.2.15.
 */
public class AdventureDAOImpl extends DAOImpl<Adventure> {
    @Override
    public int createObject(Adventure adventure) {
        return super.createObject(adventure);
    }

    public Adventure readObjectById(int id) {
        return super.readObjectById(id, Adventure.class);
    }

    public List<UserEntity> getConnectedUsers(int adventureId) {
        Session session = HibernateService.getSession();
        session.beginTransaction();
        String hql = "from UserEntity usr where usr.connectedAdventureId=:adventureId";
       /* String hql = "select user from UserEntity user" +
                " where user.connectedAdventureId=:adventureId"; */
        Query query = session.createQuery(hql);
        query.setParameter("adventureId", adventureId);
        List results = query.list();
        session.close();
        return results;
    }

    public List<Adventure> getOwnedAdventures(UserEntity user) {
        Session session = HibernateService.getSession();
        session.beginTransaction();


        String hql = "from Adventure a where a.owner.id=:userId";
        Query query = session.createQuery(hql);
        query.setParameter("userId", user.getId());
        List results = query.list();
        session.close();
        return results;

    }

    public List<Adventure> getAllowedAdventures(UserEntity user) {
        Session session = HibernateService.getSession();
        session.beginTransaction();

        String hql = "select adv from Adventure adv join adv.allowedUsers usr where usr.id=:userId";
        Query query = session.createQuery(hql);
        query.setParameter("userId", user.getId());
        List results = query.list();
        session.close();
        return results;

    }

   /* public Collection<Intersection> getIntersections(int id){
        Session session = HibernateService.getSession();
        session.beginTransaction();
        String hql = "from Intersection where =:userId";
        Query query = session.createQuery(hql);
        query.setParameter("userId", user.getId());
        List results = query.list();
        session.close();
        return results;
    }*/

    @Override
    public boolean updateObject(Adventure adventure) {
        return super.updateObject(adventure);
    }

    public boolean deleteObject(int id) {
        return super.deleteObject(id, Adventure.class);
    }

    public Collection<Adventure> getAllObjects() {
        return super.getAllObjects(Adventure.class);
    }
}
