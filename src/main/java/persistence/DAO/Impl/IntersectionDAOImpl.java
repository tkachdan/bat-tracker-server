package persistence.DAO.Impl;

import persistence.model.Intersection;
import persistence.model.Measurement;

import java.util.Collection;

/**
 * Created by dan on 26.2.15.
 */
public class IntersectionDAOImpl extends DAOImpl<Intersection> {
    @Override
    public int createObject(Intersection intersection) {
        return super.createObject(intersection);
    }

    public Intersection readObjectById(int id) {
        return super.readObjectById(id, Intersection.class);
    }

    @Override
    public boolean updateObject(Intersection intersection) {
        return super.updateObject(intersection);
    }

    public boolean deleteObject(int id) {
        return super.deleteObject(id, Intersection.class);
    }

    public boolean isObjectInDatabase(int id) {
        return super.isObjectInDatabase(id, Intersection.class);
    }

    public Collection<Intersection> getAllObjects() {
        return super.getAllObjects(Intersection.class);
    }
}
