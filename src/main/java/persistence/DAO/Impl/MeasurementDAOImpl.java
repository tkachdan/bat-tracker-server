package persistence.DAO.Impl;

import persistence.model.Measurement;

import java.util.Collection;

/**
 * Created by dan on 16.3.15.
 */
public class MeasurementDAOImpl extends DAOImpl<Measurement> {

    @Override
    public int createObject(Measurement measurement) {
        return super.createObject(measurement);
    }

    public Measurement readObjectById(int id) {
        return super.readObjectById(id, Measurement.class);
    }

    @Override
    public boolean updateObject(Measurement measurement) {
        return super.updateObject(measurement);
    }

    public boolean deleteObject(int id) {
        return super.deleteObject(id, Measurement.class);
    }

    public boolean isObjectInDatabase(int id) {
        return super.isObjectInDatabase(id, Measurement.class);
    }

    public Collection<Measurement> getAllObjects() {
        return super.getAllObjects(Measurement.class);
    }
}
