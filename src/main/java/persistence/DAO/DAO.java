package persistence.DAO;

/**
 * Created by dan on 26.2.15.
 * Data Access Object persistence layer. Provides Object access from database.
 */
import persistence.model.AbstractEntity;
import persistence.model.Measurement;

import java.util.Collection;
import java.util.List;

public interface DAO<T extends AbstractEntity> {
    /**
     * Create and save object to database.
     * @param t
     * @return
     */
    public int createObject(T t);

    /**
     * Get object by unique identificator.
     * @param id
     * @param tClass
     * @return
     */
    public T readObjectById(int id, Class<T> tClass);

    /**
     * Update object in database.
     * @param t
     * @return
     */
    public boolean updateObject(T t);

    /**
     * Delete object in database.
     * @param id
     * @param tClass
     * @return
     */
    public boolean deleteObject(int id, Class<T> tClass);

    /**
     * Get all objects from database of a particular type.
     * @param tClass
     * @return
     */

    public Collection<T> getAllObjects(Class<T> tClass);
}
