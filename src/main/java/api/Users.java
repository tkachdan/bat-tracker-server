package api;


import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import persistence.DAO.Impl.UserDAOImpl;
import persistence.model.UserEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Date;

/**
 * User entity REST API. Provides REST communication between clients and server.
 */


@Path("users")
public class Users {
    UserDAOImpl userDAO = new UserDAOImpl();

    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public String tryLogin(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
            UserEntity user = userDAO.readObjectByEmail(email);
            if (user != null) {
                if (password.equals(user.getPassword())) {
                    user.setDeviceId(jsonObject.getString("deviceId"));
                    userDAO.updateObject(user);
                    JSONObject responceJson = new JSONObject(user);
                    return responceJson.toString();
                } else {
                    return "Error.User not found!";
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "Error.User not found!";
    }

    @POST
    @Path("register")
    @Produces(MediaType.APPLICATION_JSON)
    public String register(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
            UserEntity user = new UserEntity(email, password);
            userDAO.createObject(user);
            JSONObject responceJson = new JSONObject(user);
            return responceJson.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "Error:" + e.getMessage();
        }
    }

    @Path("{userid}")
    @GET
    public String getAllPositions(@PathParam("userid") int userId) {
        UserEntity userEntity = userDAO.readObjectById(userId);
        if (userEntity == null) {
            return "Can't find user with id:" + userId;
        }
        JSONObject userJsonObject = new JSONObject(userEntity);
        return userJsonObject.toString();
    }

    @Path("setAdventure")
    @POST
    public String setConnectedAdventure(String json) {
        UserEntity user = сheckUsersCredentials(json);
        if (user != null) {
            try {
                JSONObject userJson = new JSONObject(json);
                int advId = userJson.getInt("connectedAdventureId");
                user.setConnectedAdventureId(advId);
                userDAO.updateObject(user);
                return "{result:true}";
            } catch (JSONException e) {
                return e.getMessage();
            }
        }
        return "{result:false}";
    }

    @Path("updateDeviceId")
    @POST
    public String updateDeviceId(String json) {
        UserEntity user;
        if ((user = сheckUsersCredentials(json)) != null) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                user.setDeviceId(jsonObject.getString("deviceId"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            userDAO.updateObject(user);
            return new JSONObject(user).toString();
        }
        return "{result:false}";
    }

    @Path("position")
    @POST
    public String postPosition(String json) {
        try {

            JSONObject jsonObject = new JSONObject(json);
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
            String latStr = jsonObject.getString("latitude");
            String lonStr = jsonObject.getString("longitude");
            String azimuth = jsonObject.getString("azimuth");
            UserEntity user = userDAO.readObjectByEmail(email);
            if (user != null) {
                if (password.equals(user.getPassword())) {
                    user.setLatitude(Double.parseDouble(latStr));
                    user.setLongitude(Double.parseDouble(lonStr));
                    user.setAzimuth(Double.parseDouble(azimuth));
                    userDAO.updateObject(user);
                    ConnectionTimer.connectedUsers.put(user, new Date());
                    return "{result:true}";
                } else {
                    return "Error.User not found!";
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return "Error:" + e.getMessage();
        }
        return "null";
    }


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getAllUsers() {
        Collection<UserEntity> userEntities = userDAO.getAllObjects();
        return userEntities.toString();
    }

    private UserEntity сheckUsersCredentials(String userJson) {
        try {
            JSONObject jsonObject = new JSONObject(userJson);
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
            UserEntity user = userDAO.readObjectByEmail(email);

            if (user != null && password.equals(user.getPassword())) {
                return user;
            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
