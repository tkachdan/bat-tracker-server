package api;

import persistence.model.UserEntity;

import javax.servlet.*;
import java.io.IOException;
import java.util.*;

/**
 * Checks every n seconds if client is stil connected. If not disconects the client
 */
@SuppressWarnings("serial")
public class ConnectionTimer implements Servlet {

    static Map<UserEntity, Date> connectedUsers = new HashMap<UserEntity, Date>();
    static Timer timer = new Timer();
    static TimerTask checkConnection = new TimerTask() {
        @Override
        public void run() {
            System.out.println(connectedUsers.toString());
            for (Iterator<Map.Entry<UserEntity, Date>> it = connectedUsers.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<UserEntity, Date> entry = it.next();
                if ((System.currentTimeMillis() - entry.getValue().getTime()) > 3000) {
                    System.out.println((System.currentTimeMillis() - entry.getValue().getTime()));
                    System.out.println("Disconnect:"+entry.getKey().getId()+"");
                    it.remove();
                }
            }
        }
    };

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        //timer.scheduleAtFixedRate(checkConnection, 0, 3000);
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
