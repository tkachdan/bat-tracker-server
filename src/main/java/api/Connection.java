package api;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Simple ping.
 */
@Path("connection")
public class Connection {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String ping(){
        return "{\"result\":\"ACK\"}";
    }
}
