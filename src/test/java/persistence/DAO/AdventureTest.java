package persistence.DAO;


import org.junit.Test;
import persistence.DAO.Impl.AdventureDAOImpl;
import persistence.DAO.Impl.UserDAOImpl;
import persistence.model.Adventure;
import persistence.model.UserEntity;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dan on 5.3.15.
 */
public class AdventureTest {
    UserDAOImpl userDAO = new UserDAOImpl();
    AdventureDAOImpl adventureDAO = new AdventureDAOImpl();


    @Test
    public void createTest() {
        UserEntity user = new UserEntity("a@a", "a");
        userDAO.createObject(user);
        Adventure adventure = new Adventure("first", "new_species", new Date(), user);
        UserEntity userEntity1 = new UserEntity("b@b", "b");
        userDAO.createObject(userEntity1);
        UserEntity userEntity2 = new UserEntity("c@c", "c");
        userDAO.createObject(userEntity2);
        adventure.addAllowedUsers(userEntity1);
        adventure.addAllowedUsers(userEntity2);
        int advId = adventureDAO.createObject(adventure);
    }

    @Test
    public void addAdventureTest(){
        UserEntity userEntity = userDAO.readObjectByEmail("c@c");
        Adventure  adventure = new Adventure("aaaaaaasd","species",new Date(),userEntity);
        adventureDAO.createObject(adventure);
    }

    @Test
    public void getCreateMyAdventure() {
        UserEntity user = userDAO.readObjectById(1);
        Adventure adventure = new Adventure("second", "new_species_NEW", new Date(), user);
        adventureDAO.createObject(adventure);
        List<Adventure> adventureList = adventureDAO.getOwnedAdventures(user);
        for (Adventure adv : adventureList) {
            System.out.println(adv);
        }
    }

    @Test
    public void getOwnedAdventuresTest() {
        UserEntity user = userDAO.readObjectById(1);
        Adventure adventure = new Adventure("third", "new_species_NEW", new Date(), user);
        adventureDAO.createObject(adventure);
        List<Adventure> adventureList = adventureDAO.getOwnedAdventures(user);
        for (Adventure adv : adventureList) {
            System.out.println(adv);
        }
    }

    @Test
    public void addAllowedUsers(){
        UserEntity userEntity = userDAO.readObjectByEmail("b@b");
        Adventure adventure = adventureDAO.readObjectById(13);
        adventure.addAllowedUsers(userEntity);
        adventureDAO.updateObject(adventure);

    }

    @Test
    public void allowedUsers() {
        Adventure adventure = adventureDAO.readObjectById(10);
        System.out.println("OWNER: " + adventure.getOwner());
        for (UserEntity userEntity : adventure.getAllowedUsers()) {
            System.out.println(userEntity.toString());
        }
    }
}
