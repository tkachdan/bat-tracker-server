package persistence.DAO;

import org.junit.Assert;
import org.junit.Test;
import persistence.DAO.Impl.UserDAOImpl;
import persistence.model.UserEntity;



/**
 * Created by dan on 26.2.15.
 */
public class UserEntityDAOTest {
    UserDAOImpl userDAO = new UserDAOImpl();

    @Test
    public void createTest() {
        UserEntity user = new UserEntity("mail@mail.com","pass");
        userDAO.createObject(user);
        UserEntity userEntity1 =  userDAO.readObjectById(user.getId(),UserEntity.class);
        Assert.assertEquals(user, userEntity1);
    }

    @Test
    public void readTest(){
        UserEntity user = new UserEntity("mail1","pass");
        int id = userDAO.createObject(user);
        UserEntity userEntity1 =  userDAO.readObjectById(id);
        System.out.println("USER1 : " + user.toString());
        System.out.println("USER2 : " + userEntity1.toString());
        Assert.assertEquals(user, userEntity1);
    }

    @Test
    public void readTestA(){
        UserEntity user = userDAO.readObjectByEmail("a@a");
        System.out.println(user);
    }

//    @Test
//    public void readTestID(){
//        UserEntity userEntity1 =  userDAO.readObjectById(8);
//        System.out.println("USER2 : " + userEntity1.toString());
//    }
//
//
    @Test
    public void readTestMail(){
        UserEntity userEntity1 =  userDAO.readObjectByEmail("a@a");
        System.out.println("USER2 : " + userEntity1.toString());
    }

    @Test
    public void updateTest(){
        UserEntity user = new UserEntity("mail2","pass");
        userDAO.createObject(user);
        user.setPassword("newPassword");
        userDAO.updateObject(user);
        Assert.assertEquals(user.getPassword(), "newPassword");
    }



    @Test
    public void deleteTest(){
        UserEntity user = new UserEntity("mai3l","pass");
        int id = userDAO.createObject(user);
        userDAO.deleteObject(id);
        UserEntity deletedUser = userDAO.readObjectById(id);
        Assert.assertEquals(null, deletedUser);
    }
}
